using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourCore : MonoBehaviour
{
    protected DataManager dm => DataManager.Instance;
    protected GameController gc => GameController.Instance;
    protected EnemyController enemy => EnemyController.Instance;
    protected UIController uic => UIController.Instance;

    public void RestartCoroutine(ref Coroutine coroutine, IEnumerator enumerator)
    {
        StopOneCoroutine(ref coroutine);
        coroutine = StartCoroutine(enumerator);
    }

    public void StopOneCoroutine(ref Coroutine coroutine)
    {
        if (coroutine != null) StopCoroutine(coroutine);
    }
}
