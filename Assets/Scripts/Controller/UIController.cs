using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : SingletonCore<UIController>
{
    public ObjectPooling enemyPool;
    //public ObjectPooling bulletPool;
    public SpawnManager spawnEnemy;
    public SpawnBullet spawnBullet;
}
