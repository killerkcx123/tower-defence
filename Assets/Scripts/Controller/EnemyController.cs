using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using DG.Tweening.Plugins.Core.PathCore;
using UnityEditor;
using System;

public class EnemyController : SingletonCore<EnemyController>
{
    [SerializeField] float posXMax;
    public float timeSlow;
    private void Start()
    {
        SetTypeEnemy();
        DOMoveOnPath();
    }
    private void Update()
    {
        if(this.transform.position.x >= posXMax)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
    }
    private void DOMoveOnPath()
    {
        if (TMove != null)
            TMove.Kill();
        TMove = this.transform.DOPath(gc.GetMovePathPoints(), gc.pathDuration,PathType.Linear,PathMode.Sidescroller2D);
        TMove.timeScale = timeSlow;

    }
    TweenerCore<Vector3, Path, PathOptions> TMove;
    private void SetTypeEnemy()
    {
        foreach(var itemEnemy in dm.listEnemy)
        {
            if(itemEnemy.id == dm.idEnemy)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = itemEnemy.sprite;
                gameObject.GetComponent<Animator>().runtimeAnimatorController = itemEnemy.anm;
            }
        }
        this.transform.position = gc.GetMovePathPoints()[0];
        this.transform.localScale = new Vector3(1f, 1f, 1f);
        posXMax = gc.GetMaxPosX();
    }
}

public enum EnemyType
{
    
}

