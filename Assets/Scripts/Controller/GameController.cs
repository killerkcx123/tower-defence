using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : SingletonCore<GameController>
{
    [SerializeField] Transform[] movePath;
    [SerializeField] Vector3[] movePathPoints;
    public float pathDuration;
    private void Start()
    {
        int movePathLeng = movePath.Length;
        movePathPoints = new Vector3[movePathLeng];
        for (int i = 0; i < movePathLeng; ++i)
        {
            movePathPoints[i] = movePath[i].position;
        }
    }
    public Vector3[] GetMovePathPoints()
    {
        return movePathPoints;
    }
    public float GetMaxPosX()
    {
        float posX = 0f;
        foreach(var item in GetMovePathPoints())
        {
            if(item.x > posX) posX = item.x;
        }
        return posX;
    }
}
