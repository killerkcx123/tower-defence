using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class TowerController : MonoBehaviourCore, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] float offset;
    [SerializeField] bool isOnDistance = false;
    [SerializeField] GameObject target;
    public int idGrid;
    [SerializeField] float timeShoot;
    [SerializeField] TowerType type;
    [SerializeField] GridTowerController gTC;
    [SerializeField] TowerCategory[] towerType;
    [SerializeField] GameObject towerBuild;
    [SerializeField] GameObject weaponBuild;
    [SerializeField] BulletController bullet;


    private void Update()
    {
        if(bullet == null)
            if(uic.spawnBullet.GetObjectFromSpawn() != null) bullet = uic.spawnBullet.GetObjectFromSpawn().GetComponent<BulletController>();
        WeaponDirect();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(gTC.isGridDisable(idGrid)) BuildTower();
        else 
            UpgradeTower();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public void BuildTower()
    {
        foreach (var tower in towerType)
        {
            if (tower.type.Equals(gTC.type))
            {
                type = (TowerType)(int.Parse(tower.type.ToString().Replace("tower_", "")) - 1);
                towerBuild.GetComponent<Image>().sprite = tower.towerLevel[gTC.level];
                weaponBuild.GetComponent<Image>().sprite = tower.weaponLevel[gTC.level];
                gTC.ChooseDone(idGrid);
                
            }
        }
        DistanceShoot();
    }
    public void UpgradeTower()
    {
        Debug.Log("Upgrade Tower");
    }

    void ShootEnemy()
    {
        timeShoot -= Time.deltaTime;
        if(timeShoot <= 0f)
        {
            timeShoot = 2f;
            if (isOnDistance)
            {
                bullet.SetBulletData(weaponBuild.transform, (int)type);
                Vector2 posTarget = target.transform.position;
                StartCoroutine(bullet.MoveObject(posTarget, 0.2f));
                bullet = null;
            }
        }
        
    }

    public void WeaponDirect()
    {
        if (isOnDistance)
        {
            Vector3 targetPos = target.transform.position;
            Vector3 thisPos = weaponBuild.transform.position;
            targetPos.x = targetPos.x - thisPos.x;
            targetPos.y = targetPos.y - thisPos.y;
            float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
            weaponBuild.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - offset));
            if (bullet != null)
            {
                //bullet.BulletDirect(target, offset);
                bullet.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - offset));
            }
        }
    }

    public void DistanceShoot()
    {
        switch (type)
        {
            case TowerType.tower_1:
                this.GetComponent<CapsuleCollider2D>().size = new Vector2(360, 1);
                break;
            case TowerType.tower_2:
                this.GetComponent<CapsuleCollider2D>().size = new Vector2(450, 1);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isOnDistance && collision.gameObject.tag.Equals("Enemy"))
        {
            isOnDistance = true;
            target = collision.gameObject;
            Debug.Log("target is on Distance");
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!isOnDistance && collision.gameObject.tag.Equals("Enemy"))
        {
            isOnDistance = true;
            target = collision.gameObject;
            Debug.Log("target is on Distance");
        }
        ShootEnemy();
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            isOnDistance = false;
            Debug.Log("target is out Distance");
        }
    }
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawSphere(sphere, 1);
    //}
}
public enum TowerType
{
    tower_1, 
    tower_2, 
    tower_3, 
    tower_4, 
    tower_5, 
    tower_6
}
[Serializable]
public class TowerCategory
{
    public string type;
    public Sprite[] towerLevel;
    public Sprite[] weaponLevel;
}