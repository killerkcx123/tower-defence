using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

public class BulletController : MonoBehaviourCore
{
    [SerializeField] GameObject bulletTower;
    [SerializeField] Bullet[] typeBullet;

    public void SetBulletData(Transform build,int value)
    {
        gameObject.SetActive(true);
        transform.position = build.position;
        foreach (Bullet bullet in typeBullet)
        {
            if(bullet.id == value) bulletTower.GetComponent<SpriteRenderer>().sprite = bullet.levelBullet[value];
        }
    }

    public IEnumerator MoveObject(Vector2 targetPos, float duration)
    {
        float time = 0;
        float rate = 1 / duration;
        Vector2 startPos = transform.localPosition;
        while (time < 1)
        {
            time += rate * Time.deltaTime;
            this.transform.localPosition = Vector2.Lerp(startPos, targetPos, time);
            yield return 0;
        }
    }


    public void BulletDirect(GameObject target,float offset)
    {
            Vector3 targetPos = target.transform.position;
            Vector3 thisPos = gameObject.transform.position;
            targetPos.x = targetPos.x - thisPos.x;
            targetPos.y = targetPos.y - thisPos.y;
            float angle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
            gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - offset));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            gameObject.SetActive(false);
            collision.gameObject.SetActive(false);
        }
    }
}
[Serializable]
public class Bullet
{
    public int id;
    public float timeShoot;
    public float[] dame;
    public Sprite[] levelBullet;
}
