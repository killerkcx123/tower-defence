using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class HealController : MonoBehaviour
{
    [SerializeField] Transform enemy;
    [SerializeField] Slider slider;
    [SerializeField] float value;

    private void Start()
    {
        transform.position = enemy.position + new Vector3(0,0.276f,0);
    }

    public void SetHeal(float val)
    {
        value = val;
        slider.maxValue = value;
        slider.value = value;
    }

    public void ChangeHeal(float val)
    {
        value = val;
        slider.value = value;
    }
}
