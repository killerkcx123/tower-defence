using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTowerController : MonoBehaviourCore
{
    public string type ="";
    public int level = -1;
    public bool build = false;
    [SerializeField] GameObject[] grid;
    [SerializeField] GameObject[] unlockGrid;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < grid.Length; i++)
        {
            grid[i].SetActive(false);
            unlockGrid[i].SetActive(false);
        }
    }
    public void OnBuyTower(ScaleButton p)
    {
        switch (p.type)
        {
            case "tower_1":
                type = p.type;
                level = p.id;
                break;
            case "tower_2":
                type = p.type;
                level = p.id;
                break;
            case "Cancel":
                type = p.type;
                level = -1;
                break;
            case "Destroy":
                type = p.type;
                level = -1;
                break;
        }
        for (int i = 0; i < grid.Length; i++)
        {
            build = true;
            if(grid[i].activeSelf == false) unlockGrid[i].SetActive(true);
        }
    }
    public void ChooseDone(int index)
    {
        if (build && grid[index].activeSelf == false) 
        {
            unlockGrid[index].SetActive(false);
            grid[index].SetActive(true);
            foreach (GameObject unlockitem in unlockGrid)
            {
                unlockitem.SetActive(false);
            }
        }
        ResetData();
    }
    public void ResetData()
    {
        build = false;
        type = "";
        level = -1;
    }

    public bool isGridDisable(int index)
    {
        if(grid[index].activeSelf == false)
        {
            return true;
        }
        return false;
    }
}
