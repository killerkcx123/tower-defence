using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviourCore
{
    public GameObject prefab;
    public int poolSize;
    public int index = 0;
    [SerializeField] List<GameObject> objectList;

    void Start()
    {
        objectList = new List<GameObject>();

        for (int i = 0; i < poolSize; i++)
        {
            GameObject obj = Instantiate(prefab);
            obj.SetActive(false);
            objectList.Add(obj);
        }
    }

    public GameObject GetObjectFromPool(Transform pos)
    {
        for (int i = 0; i < objectList.Count; i++)
        {
            if (!objectList[i].activeInHierarchy)
            {
                index += 1;
                objectList[i].transform.position = pos.position;
                return objectList[i];
            }
        }

        // If no available object in pool, create a new one
        GameObject newObj = Instantiate(prefab, pos.position, prefab.transform.rotation);
        newObj.SetActive(false);
        objectList.Add(newObj);
        index += 1;
        return newObj;
    }

    public void ReturnObjectToPool(GameObject obj)
    {
        objectList.Remove(obj);
        index -= 1;
        obj.SetActive(false);
        obj.transform.position = transform.position;
        objectList.Add(obj);
    }
}
