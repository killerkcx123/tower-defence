using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class DataManager : SingletonCore<DataManager>
{
    public EnemyData[] listEnemy;
    public int idEnemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    } 
}

[Serializable]
public class GameData
{
    
}

[Serializable]
public class EnemyData
{
    public int id;
    public float maxHP;
    public Sprite sprite;
    public AnimatorController anm;
}
