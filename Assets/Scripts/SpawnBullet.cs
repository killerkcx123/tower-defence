using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviourCore
{
    [SerializeField] List<Transform> posSpawn = new List<Transform>();
    [SerializeField] GameObject prefab;
    [SerializeField] List<GameObject> bullets;
    [SerializeField] int total;
    //[SerializeField] int totalList = 0;
    [SerializeField] int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        SpawnBullets();
    }

    void SpawnBullets()
    {  
        if(index < total)
        {
            GameObject newObj = Instantiate(prefab, posSpawn[0].position,prefab.transform.rotation);
            index += 1;
            newObj.SetActive(false);
            bullets.Add(newObj);
        }
    }

    public GameObject GetObjectFromSpawn()
    {
        foreach(var item in bullets)
        {
            if(item.activeSelf == false)
            {
                return item;
            }
        }
        index = 0;
        SpawnBullets();
        return GetObjectFromSpawn();
    }
}
