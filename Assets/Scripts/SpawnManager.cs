using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviourCore
{
    [SerializeField] List<Transform> posSpawn = new List<Transform>();
    public int total;
    [SerializeField] float timeSpawn = 0f;
    // Update is called once per frame
    void Update()
    {
        SpawnEnemy();
    }
    void SpawnEnemy()
    {
        if (uic.enemyPool.index < total)
        {
            timeSpawn += Time.deltaTime;
            if(timeSpawn > 1f)
            {
                timeSpawn = 0f;
                int index = Random.Range(0, posSpawn.Count);
                GameObject enemy = uic.enemyPool.GetObjectFromPool(posSpawn[0]);
                enemy.SetActive(true);
                
            }
        }
    }
}
